package com.example.primewater;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class Register extends AppCompatActivity {

    private Button btn_create_account, btn_goto_login;
    private EditText editText_acc_no, editText_last_name, editText_first_name, editText_email, editText_Password, editText_retypePassword;
    private ProgressBar progressBar;

    private FirebaseAuth auth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        auth = FirebaseAuth.getInstance();

        btn_create_account = (Button) findViewById(R.id.btn_login);
        btn_goto_login = (Button) findViewById(R.id.btn_goto_login);
        editText_acc_no = (EditText) findViewById(R.id.editText_acc_no);

        editText_last_name = (EditText) findViewById(R.id.editText_last_name);
        editText_first_name= (EditText) findViewById(R.id.editText_first_name);
        editText_email= (EditText) findViewById(R.id.editText_email);
        editText_Password=(EditText) findViewById(R.id.editText_Password);
        editText_retypePassword= (EditText) findViewById(R.id.editText_retypePassword);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);



        //go to create account
        btn_goto_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Register.this, Login.class));
            }
        });


        btn_create_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String last_name = editText_last_name.getText().toString().trim();
                String first_name = editText_first_name.getText().toString().trim();
                String email = editText_email.getText().toString().trim();
                String password = editText_Password.getText().toString().trim();
                String passwordretype = editText_retypePassword.getText().toString().trim();

                if (TextUtils.isEmpty(email)) {
                    Toast.makeText(getApplicationContext(), "Enter email address!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(password)) {
                    Toast.makeText(getApplicationContext(), "Enter password!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(password)) {
                    Toast.makeText(getApplicationContext(), "Retye the password!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (password.length() < 6) {
                    Toast.makeText(getApplicationContext(), "Password too short, enter minimum 6 characters!", Toast.LENGTH_SHORT).show();
                    return;
                }

                progressBar.setVisibility(View.VISIBLE);
               // create user, firebase automatically encrypts the user password so remember it
                auth.createUserWithEmailAndPassword(email, password)
                        .addOnCompleteListener(Register.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                            Toast.makeText(Register.this, "Success!" + task.isSuccessful(), Toast.LENGTH_SHORT).show();
                                progressBar.setVisibility(View.GONE);
                                // If sign in fails, display a message to the user. If sign in succeeds
                                // the auth state listener will be notified and logic to handle the
                                // signed in user can be handled in the listener.
                                if (!task.isSuccessful()) {
                                    Toast.makeText(Register.this, "Authentication failed." + task.getException(),
                                            Toast.LENGTH_SHORT).show();
                                } else {
                                    startActivity(new Intent(Register.this, MainActivity.class));
                                    finish();
                                }
                            }
                        });
            }
        });
    }

}
